<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::all();
        $products = Product::paginate(15);

        return view('home.index', compact('products', 'categories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories()
    {
        $categories = Category::all();

        return view('home.categories', compact('categories'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categoryProducts($category, Request $request)
    {
        $query = $request->get('query');

        if ($query) {
            $products = Product::search($query)
                ->where('category_id', $category)
                ->paginate(15);
        } else {
            $products = Product::where('category_id', $category)
                ->paginate(15);
        }

        return view('home.products', compact('products'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function products(Request $request)
    {
        $query = $request->get('query');

        if($query){
            $products = Product::search($query)
                                ->paginate(15);
        }
        else {
            $products = Product::paginate(15);
        }

        return view('home.products', compact('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function showProduct(Product $product)
    {
        return view('home.product', compact('product'));
    }

}
