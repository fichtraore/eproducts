<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index');
Route::get('categories', 'HomeController@categories')->name('home.categories');
Route::get('products/{query?}', 'HomeController@products')->name('home.products');
Route::get('products/category/{category}/{query?}', 'HomeController@categoryProducts')->name('home.category.products');
Route::get('products/detail/{product}', 'HomeController@showProduct')->name('home.products.show');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', 'UserController@show')->name('profile');
    Route::get('/profile/edit', 'UserController@edit')->name('profile.edit');
    Route::put('/profile/update', 'UserController@update')->name('profile.update');
});

//Resources
Route::group(['prefix' => 'backend', 'middleware' => 'role:admin'], function () {
    Route::get('/', 'Backend\HomeController@index')->name('backend');
    Route::resource('categories', 'Backend\CategoryController');
    Route::resource('products', 'Backend\ProductController');
});
