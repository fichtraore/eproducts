<?php

use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
                    'name' => 'admin',
                    'description' => 'Application administrator',
                ]);
        User::create([
            'name' => 'Trafalgar Law',
            'email' => 'law@corazon.com',
            'password' => bcrypt('secret'),
            'role_id' => $role->id,
        ]);
    }
}
