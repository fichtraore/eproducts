@extends('layouts.app')

@section('content')
<div class="container">
  <h2 class="page-header">{{ __('Profil') }}</h2>
  <div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">\
					<title>Cover</title>
					<rect width="100%" height="100%" fill="#e2e6ea"></rect>
				</svg>
				<div class="card-body">

						<h5 class="card-title">{{ $user->name }}</h5>
						<h6 class="card-subtitle mb-2 text-muted">{{ $user->email }}</h6>
						<h6 class="card-subtitle mb-2 text-muted">{{ ucfirst(optional($user->role)->name) }}</h6>
						<p class="card-text">{{ $user->description }}</p>

						
						<a href="{{ route('profile.edit')}}" class="btn btn-primary" role="button">{{ __('Editer') }}</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
