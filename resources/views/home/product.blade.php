@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">\
					<title>Cover</title>
					<rect width="100%" height="100%" fill="#e2e6ea"></rect>
				</svg>
				<div class="card-body">

						<h5 class="card-title">{{ $product->name }}</h5>
						<h6 class="card-subtitle mb-2 text-muted">{{ optional($product->category)->name }}</h6>
						<p class="card-text">{{ $product->description }}</p>
				</div>
			</div>
		</div>
		
		<div class="col-md-2 offset-md-8">
				<a href="{{ url()->previous() }}" class="btn btn-light right" role="button">{{ __('Retour') }}</a>
		</div>
	</div>
</div>
@endsection
