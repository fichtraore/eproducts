@extends('layouts.app')

@section('content')
<div class="container home">

    <section class="categories">
      <!-- START CATEGORIES LISTING -->
      <h2 class="page-header">{{ __('Catégories') }}</h2>

      <div class="row">
        @foreach ($categories as $category)
          <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="{{ route('home.category.products', $category->id) }}" class="simple-link">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">{{ $category->name }}</span>
                  <span class="info-box-number">{{ $category->products->count() }}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </a>
          </div>
          <!-- /.col -->
          @if ($loop->iteration == 5)
            <div class="col-md-3 col-sm-6 col-xs-12">
              <a href="{{ route('home.categories') }}" class="simple-link">
                <div class="info-box bg-aqua read-more">
                    {{ __('Voir toutes les catégories') }}
                </div>
                <!-- /.info-box -->
              </a>
              @break
            </div>
          @endif
        @endforeach
      </div>
    </section>

    <section >
      <!-- START PRODUCTS LISTING -->
      <h2 class="page-header">{{ __('Produits') }}</h2>
      <div class="row">
        @foreach ($products as $product)
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="card">
            <a href="{{ route('home.products.show', $product->id) }}" class="simple-link">
              <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">\
                <title>Cover</title>
                <rect width="100%" height="100%" fill="#e2e6ea"></rect>
              </svg>
              <div class="card-body">
                <h5 class="card-title">{{ $product->name }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{ optional($product->category)->name }}</h6>
                <p class="card-text">
                  {{ str_limit($product->description, 100) }}
                </p>
              </div>
            </a>
          </div>
          </div>
        @endforeach

        {{ $products->links() }}
      </div>
    </section>

  </div>
</div>
@endsection
