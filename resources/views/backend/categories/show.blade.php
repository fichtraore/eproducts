@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">\
					<title>Cover</title>
					<rect width="100%" height="100%" fill="#00c0ef"></rect>
				</svg>
                <div class="card-body">
                    <h5 class="card-title">{{ $category->name }}</h5>
                    <p class="card-text">{{ $category->description }}</p>
                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary" role="button">{{ __('Editer') }}</a>
                    <a href="{{ route('categories.destroy', $category->id) }}" class="btn btn-danger" role="button">{{ __('Supprimer') }}</a>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <a href="{{ route('categories.index') }}" class="btn btn-light float-right" role="button">{{ __('Liste des catégories') }}</a>
		</div>
    </div>
</div>
@endsection
