@extends('layouts.backend')

@section('content')
<div class="container">
     <section class="categories">
       <!-- START CATEGORIES LISTING -->
       <h2 class="page-header">
        {{ __('Catégories') }}
        <a href="{{ route('categories.create') }}" class="btn btn-outline-success add-btn" role="button">{{ __('Ajouter') }}</a>
      </h2>
      
      <div class="row">
        @foreach ($categories as $category)
          <div class="col-md-4 col-sm-6 col-xs-12">
            <a href="{{ route('categories.show', $category->id) }}" class="simple-link">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-flag-o"></i></span>

                <div class="info-box-content">
                  <span class="info-box-text">{{ $category->name }}</span>
                  <span class="info-box-number">{{ $category->products->count() }}</span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </a>
          </div>
          <!-- /.col -->
        @endforeach
      </div>
     </section>
</div>
@endsection
