@extends('layouts.backend')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">\
					<title>Cover</title>
					<rect width="100%" height="100%" fill="#e2e6ea"></rect>
					<text x="50%" y="50%" fill="#212529" dy=".3em">{{ $product->name }}</text>
				</svg>
				<div class="card-body">

						<h5 class="card-title">{{ $product->name }}</h5>
						<h6 class="card-subtitle mb-2 text-muted">{{ optional($product->category)->name }}</h6>
						<p class="card-text">{{ $product->description }}</p>

						<form action="{{ route('products.destroy', $product->id)}}" method="post">
							@csrf
							@method('DELETE')
							<a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary" role="button">{{ __('Editer') }}</a>
							<button class="btn btn-danger" type="submit">{{ __('Supprimer') }}</button>
						</form>
				</div>
			</div>
		</div>
		<div class="col-md-8">
				<a href="{{ route('products.index') }}" class="btn btn-light float-right" role="button">{{ __('Liste des produits') }}</a>
		</div>
	</div>
</div>
@endsection
