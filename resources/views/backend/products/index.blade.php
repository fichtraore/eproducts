@extends('layouts.backend')

@section('content')
<div class="container">

  <section>
    <!-- START PRODUCTS LISTING -->
    <h2 class="page-header">
      {{ __('Produits') }}
      <a href="{{ route('products.create') }}" class="btn btn-outline-success add-btn" role="button">{{ __('Ajouter') }}</a>
    </h2>
    <div class="row">
      @foreach ($products as $product)
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="card">
            <a href="{{ route('products.show', $product->id) }}" class="simple-link">
              <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap">\
                <title>Cover</title>
                <rect width="100%" height="100%" fill="#e2e6ea"></rect>
              </svg>
              <div class="card-body">
                <h5 class="card-title">{{ $product->name }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">{{ optional($product->category)->name }}</h6>
                <p class="card-text">
                  {{ str_limit($product->description, 100) }}
                </p>
              </div>
            </a>
          </div>
        </div>
      @endforeach

      {{ $products->links() }}

    </div>

  </section>
</div>
@endsection
